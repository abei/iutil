package com.qinf.study.iutil.copier.bean;

import com.qinf.study.iutil.copier.Copy;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by qinf on 2020-03-07.
 */
@Data
public class Source extends InheritedSource {
    private Integer numberInt;
    private float numberFloat;
    private boolean isBooleanMale;
    private LinkedList referenceCollection;
    private ConcurrentHashMap referenceMap;
    private String referenceString;
    private String referenceInheritedString2;
    private Inner referenceInnerBean;
    @Copy(TO = "referenceStringTarget")
    private String referenceStringCopySource;
}
