package com.qinf.study.iutil.copier.bean;

import lombok.Data;

/**
 * Created by qinf on 2020-03-07.
 */
@Data
public class InheritedTarget {
    private String referenceInheritedString2;
}
