package com.qinf.study.iutil.copier.bean;

import lombok.Data;

/**
 * Created by qinf on 2020-03-08.
 */
@Data
public class DeepInner {
    private String deepInnerString;
}
