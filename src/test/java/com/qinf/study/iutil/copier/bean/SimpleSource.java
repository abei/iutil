package com.qinf.study.iutil.copier.bean;

import lombok.Data;

/**
 * Created by qinf on 2020-08-06.
 */
@Data
public class SimpleSource {
    private Integer age;
    private String name;
}
