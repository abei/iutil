package com.qinf.study.iutil.copier.bean;

import lombok.Data;

/**
 * Created by qinf on 2020-03-07.
 */
@Data
public class InheritedSource {
    private String referenceInheritedString1;
}
