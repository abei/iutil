package com.qinf.study.iutil.copier;

import com.qinf.study.iutil.copier.bean.*;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2020-03-07.
 */
public class BeanCopyUtilTest {

    @Test
    public void testPrimitiveDataTypeCopy() {
        LinkedList tmpList = new LinkedList();
        tmpList.add("Hello");
        tmpList.add("World");
        //source.setReferenceCollection(Arrays.asList(new String[]{"Hello", "World"}));
        //ConcurrentHashMap tmpMap = (ConcurrentHashMap) tmpList.stream().collect(Collectors.toMap(v -> v, v -> v + "Value"));
        ConcurrentHashMap tmpMap = new ConcurrentHashMap(5);
        tmpMap.put("Hellow", "HelloValue");
        tmpMap.put("World", "WorldValue");
        Source source = new Source();
        source.setNumberInt(32);
        source.setNumberFloat(10000F);
        source.setBooleanMale(true);
        source.setReferenceCollection(tmpList);
        source.setReferenceMap(tmpMap);
        Target target = new Target();

        BeanCopyUtil.copy(source, target);
        System.out.println(target);
    }

    @Test
    public void testBoxingDataTypeCopy() {
        Source source = new Source();
        source.setNumberInt(32);
        source.setNumberFloat(10000F);
        Target target = new Target();
        BeanCopyUtil.copy(source, target);
        System.out.println(target);
    }

    @Test
    public void testNullCopy() {
        Integer source = null;
        int target;
        /** As we know, it's impossible to assign. */
        target = source;
        System.out.println(target);
    }

    @Test
    public void testForceCopy() {
        Source source = new Source();
        source.setNumberInt(32);
        source.setNumberFloat(10000F);
        source.setBooleanMale(true);
        source.setReferenceString(null);
        Target target = new Target();
        target.setReferenceString("I Contain A Value.");
        BeanCopyUtil.copy(source, target);
        System.out.println(target);
    }

    @Test
    public void testBooleanIsPrefixCopy() {
        Source source = new Source();
        source.setBooleanMale(true);
        Target target = new Target();
        BeanCopyUtil.copy(source, target);
        System.out.println(target);
    }

    @Test
    public void testInheritedBeanCopy() {
        Source source = new Source();
        source.setNumberInt(32);
        source.setNumberFloat(10000F);
        source.setBooleanMale(true);
        source.setReferenceInheritedString1("Inherited Field 1");
        source.setReferenceInheritedString2("Inherited Field 2");
        Target target = new Target();
        BeanCopyUtil.copy(source, target);
        System.out.println(target);
    }

    @Test
    public void testInnerBeanCopy() {
        Source source = new Source();
        source.setNumberInt(32);
        source.setNumberFloat(10000F);
        source.setBooleanMale(true);
        source.setReferenceInheritedString1("Inherited Field 1");
        source.setReferenceInheritedString2("Inherited Field 2");
        DeepInner deepInner = new DeepInner();
        deepInner.setDeepInnerString("Deep Inner Field");
        Inner inner = new Inner();
        inner.setInnerInteger(1000);
        inner.setInnerList(Arrays.asList(new String[]{"Inner Field 1", "Inner Field 2"}));
        inner.setDeepInner(deepInner);
        source.setReferenceInnerBean(inner);
        Target target = new Target();
        BeanCopyUtil.copy(source, target);
        System.out.println(target);
    }

    @Test
    public void testCopyMode() {
        Source source = new Source();
        source.setNumberInt(32);
        source.setNumberFloat(10000F);
        source.setBooleanMale(true);
        source.setReferenceInheritedString1("Inherited Field 1");
        source.setReferenceInheritedString2("Inherited Field 2");
        source.setReferenceInnerBean(null);
        Target target = new Target();
        DeepInner deepInner = new DeepInner();
        deepInner.setDeepInnerString("Deep Inner Field");
        Inner inner = new Inner();
        inner.setInnerInteger(1000);
        inner.setInnerList(Arrays.asList(new String[]{"Inner Field 1", "Inner Field 2"}));
        inner.setDeepInner(deepInner);
        target.setReferenceInnerBean(inner);
        BeanCopyUtil.copy(source, target, BeanCopyUtil.BEAN_TO_BEAN_IGNORE_NULL);
        System.out.println(target);
    }

    @Test
    public void testCopyAnnotation() {
        Source source = new Source();
        source.setNumberInt(32);
        source.setNumberFloat(10000F);
        source.setBooleanMale(true);
        source.setReferenceInheritedString1("Inherited Field 1");
        source.setReferenceInheritedString2("Inherited Field 2");
        source.setReferenceInnerBean(null);
        source.setReferenceStringCopySource("Source Field");
        Target target = new Target();
        DeepInner deepInner = new DeepInner();
        deepInner.setDeepInnerString("Deep Inner Field");
        Inner inner = new Inner();
        inner.setInnerInteger(1000);
        inner.setInnerList(Arrays.asList(new String[]{"Inner Field 1", "Inner Field 2"}));
        inner.setDeepInner(deepInner);
        target.setReferenceInnerBean(inner);
        BeanCopyUtil.copy(source, target, BeanCopyUtil.BEAN_TO_BEAN_IGNORE_NULL);
        System.out.println(target);
    }

    @Test
    public void testCopyMapToBean() {
        Map<String, Object> map = new HashMap<>(5);
        map.put("numberInt", 10);
        map.put("numberFloat", new Float(20.5));
        map.put("reference_string", "snake case source");
        map.put("referenceString_Target", "not know");
        Target bean = BeanCopyUtil.copy(map, new Target(), BeanCopyUtil.MAP_TO_BEAN);
        System.out.println(bean);
    }

    @Test
    public void testToCamelCase() {
        String camelCase = BeanCopyUtil.toCamelCase("hello_worLd");
        System.out.println(camelCase);
    }

    @Test
    public void testStringNumberInterconversion() {
        Map<String, Object> map = new HashMap<>(5);
        map.put("numberInt", "1000");
        map.put("isBooleanMale", "trUe");
        map.put("referenceDate", new Date());
        map.put("customizedDate", "2020-04-02 19:22:36");
        Target bean = BeanCopyUtil.copy(map, new Target(), BeanCopyUtil.MAP_TO_BEAN_CAMEL_CASE);
        System.out.println(bean);
    }

    @Test
    public void testCompatibleClassObjectCopy() {
        Map<String, Object> map = new HashMap<>(5);
        map.put("referenceDate", new Date());
        map.put("customizedDate", "2020-04-02 19:22:36");
        map.put("referenceString", new Double(2000330333322D));
        map.put("referenceInheritedString1", new Float(2222.4433));
        Target bean = BeanCopyUtil.copy(map, Target.class);
        System.out.println(bean);
    }

    @Test
    public void testCollectorsToMapRestriction() {
        Map<String, Object> map = new HashMap<>(5);
        map.put(null, "value1");
        map.put("key2", "value2");
        Map<String, Object> copiedMap = map.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
        /*Map<String, Object> copiedMap = map.entrySet()
                .stream().collect(
                        HashMap<String, Object>::new,
                        (m, e) -> m.put(e.getKey() + "_", e.getValue()),
                        HashMap::putAll);*/
        System.out.println(copiedMap);
    }

    @Test
    public void testInstanceWhenCopy() {
        SimpleSource source = new SimpleSource();
        source.setAge(10);
        source.setName("Tom");
        //SimpleTarget target = BeanCopyUtil.copy(source, SimpleTarget.class);
        SimpleTarget target = BeanCopyUtil.forceCopy(source, new SimpleTarget());
        System.out.println(target);
    }
}
