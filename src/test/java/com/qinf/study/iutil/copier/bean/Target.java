package com.qinf.study.iutil.copier.bean;

import com.qinf.study.iutil.copier.Copy;
import com.qinf.study.iutil.copier.DateFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by qinf on 2020-03-07.
 */
@Data
public class Target extends InheritedTarget {
    private int numberInt;
    private Float numberFloat;
    private boolean isBooleanMale;
    private List referenceCollection;
    private Map referenceMap;
    private String referenceString;
    private String referenceInheritedString1;
    private Inner referenceInnerBean;
    @Copy(FROM = "referenceString_Target")
    private String referenceStringTarget;
    @DateFormat(FORMAT = "yyyy-MM-dd")
    private String referenceDate;
    @DateFormat(FORMAT = "yyyy-MM-dd")
    private Date customizedDate;
}
