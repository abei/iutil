package com.qinf.study.iutil.copier.bean;

import lombok.Data;

import java.util.List;

/**
 * Created by qinf on 2020-03-08.
 */
@Data
public class Inner {
    private Integer innerInteger;
    private List innerList;
    private DeepInner deepInner;
}
