package com.qinf.study.iutil.copier.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by qinf on 2020-08-06.
 */
@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
public class SimpleTarget implements Serializable {

    //private static final long serialVersionUID = 898627533934476737L;
    private static final Long serialVersionUID = 898627533934476737L;

    /*private SimpleTarget() {
    }*/

    public SimpleTarget() {
    }

    /*public SimpleTarget(Integer age) {
        this.age = age;
    }*/

    private Integer age;
    private String name;
}
