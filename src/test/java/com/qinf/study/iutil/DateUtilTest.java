package com.qinf.study.iutil;

import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by qinf on 2020-10-29.
 */
public class DateUtilTest {

    @Test
    public void testBeforeDaysOfDate() {
        Date currentTime = new Date();
        Date from = DateUtil.beforeDaysOfDate(currentTime, 3);
        Date to = DateUtil.beforeDaysOfDate(currentTime, 0);
        System.out.println("The two date is same date: " + DateUtil.isSameDay(from, to));
        System.out.println("The date range in single day: " + DateUtil.getEarliestTime(from) + " ~ " + DateUtil.getLatestTime(to));
        String fmFrom = DateUtil.date2String(from, "yyyyMMddHHmmss");
        String fmTo = DateUtil.date2String(to, "yyyyMMddHHmmss");
        System.out.println("The date from " + fmFrom + " to " + fmTo);

        Integer daysFrom = 10, daysTo = 5;
        this.swap(daysFrom, daysTo);
    }

    private void swap(Integer daysFrom, Integer daysTo) {
        System.out.println("The date from " + daysFrom + " to " + daysTo);
        daysFrom = daysFrom ^ daysTo ^ (daysTo = daysFrom);
        System.out.println("The date from " + daysFrom + " to " + daysTo);
    }

    @Test
    public void testDiffDays() {
        long case1DiffDays = DateUtil.diffDays(DateUtil.string2Date("2020-07-10 22:59:59"),
                DateUtil.string2Date("2020-11-10 01:59:59"));
        System.out.println("Case1, The diff days is: " + case1DiffDays);
        long case2DiffDays = DateUtil.diffDays(DateUtil.string2Date("2020-07-10 23:59:59"),
                DateUtil.string2Date("2019-07-10 23:59:59"));
        System.out.println("Case2, The diff days is: " + case2DiffDays);
        long case3DiffDays = DateUtil.diffDays(DateUtil.string2Date("2020-10-28 23:59:59"),
                DateUtil.string2Date("2020-10-29 00:59:59"));
        System.out.println("Case3, The diff days is: " + case3DiffDays);
    }

    @Test
    public void testDiffDaysByMis() {
        long case1DiffDays = DateUtil.diffDaysByMis(DateUtil.string2Date("2019-10-10 23:59:59"),
                DateUtil.string2Date("2020-11-10 23:59:59"));
        System.out.println("Case1, The diff days by mis is: " + case1DiffDays);
        long case2DiffDays = DateUtil.diffDaysByMis(DateUtil.string2Date("2020-10-27 23:59:59"),
                DateUtil.string2Date("2020-10-29 23:59:59"));
        System.out.println("Case2, The diff days by mis is: " + case2DiffDays);
        long case3DiffDays = DateUtil.diffDaysByMis(DateUtil.string2Date("2020-10-28 23:59:59"),
                DateUtil.string2Date("2020-10-29 22:59:59"));
        System.out.println("Case3, The diff days by mis is: " + case3DiffDays);
    }

    @Test
    public void testGetFirstDayOfCurrentMonth() {
        System.out.println("First day of current month is: " + DateUtil.date2String(DateUtil.getFirstDayOfCurrentMonth(), "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void testGetFirstDayOfLastMonth() {
        System.out.println("First day of last month is: " + DateUtil.date2String(DateUtil.getFirstDayOfLastMonth(), "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void testTimestamp2String() {
        System.out.println("Time stamp to string: " + DateUtil.timestamp2String(Timestamp.valueOf(LocalDateTime.now())));
    }

    @Test
    public void testSecToHMS() {
        System.out.println("Convert duration in seconds to HMS: " + DateUtil.secToHMSFormat(100));
    }
}
