package com.qinf.study.iutil;

import lombok.SneakyThrows;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by qinf on 2021-07-26.
 */
public class FileUtilTest {

    private static final String SRC_ZIP_FILE_PATH = "D:/abei.zip";

    private static final String SRC_XML_FILE_PATH = "D:/abei.txt";

    @SneakyThrows
    @Test
    public void testGetFileTypeByBytes() {
        try (InputStream ins = new FileInputStream(SRC_ZIP_FILE_PATH)) {
            byte[] bytes = new byte[30000];
            ins.read(bytes);
            System.out.println("Get the file type by bytes: " + FileUtil.getFileTypeByBytes(bytes));
        }
    }

    @SneakyThrows
    @Test
    public void testGetFileTypeByIns() {
        try (InputStream ins = new FileInputStream(SRC_ZIP_FILE_PATH)) {
            System.out.println("Get the file type by ins: " + FileUtil.getFileTypeByIns(ins));
        }
    }

    @Test
    public void testZipMultiFiles() {
        byte[] file1Content = {0, 1, 0, 99, 100, 36, 43};
        byte[] file2Content = {12, 37, 22, 55, 33};
        List<FileUtil.InnerFile> multiFiles = new ArrayList<>();
        FileUtil.InnerFile file1 = new FileUtil.InnerFile();
        file1.setFileName("inner-file-01.pdf");
        file1.setFileContent(file1Content);
        FileUtil.InnerFile file2 = new FileUtil.InnerFile();
        file2.setFileName("inner-file-02.jpg");
        file2.setFileContent(file2Content);
        multiFiles.add(file1);
        multiFiles.add(file2);
        FileUtil.zipMultiFiles("compressed_file.zip", multiFiles);
        byte[] result = FileUtil.zipMultiFiles(multiFiles);
        System.out.println(result);
    }

    @Test
    public void testUnzipFile() {
        try (InputStream ins = new FileInputStream(SRC_ZIP_FILE_PATH);
             ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int nRead;
            byte[] data = new byte[16384];
            while ((nRead = ins.read(data, 0, data.length)) != -1) {
                baos.write(data, 0, nRead);
            }
            byte[] unzippedBytes = FileUtil.unzipFile(FileUtil.InnerFile.builder()
                    .fileName("abei.xml")
                    .fileContent(baos.toByteArray())
                    .build());
            System.out.println(new String(unzippedBytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSplitZippedMultiFilesBytes() {
        try (InputStream ins = new FileInputStream(SRC_ZIP_FILE_PATH);
             ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int nRead;
            byte[] data = new byte[16384];
            while ((nRead = ins.read(data, 0, data.length)) != -1) {
                baos.write(data, 0, nRead);
            }
            List<FileUtil.InnerFile> splittedFiles = FileUtil.splitZippedMultiFilesBytes(baos.toByteArray());
            System.out.println(splittedFiles);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseXmlBytes() {
        try (InputStream ins = new FileInputStream(SRC_XML_FILE_PATH);
             ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int nRead;
            byte[] data = new byte[16384];
            while ((nRead = ins.read(data, 0, data.length)) != -1) {
                baos.write(data, 0, nRead);
            }
            Map<String, Object> parsedXmlMap = FileUtil.parseXmlBytes(baos.toByteArray());
            System.out.println(parsedXmlMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetFileEncodingByIns() {
        try (InputStream ins = new FileInputStream(SRC_XML_FILE_PATH)) {
            Charset charset = FileUtil.getFileEncodingByIns(ins);
            System.out.println(charset);
            //InputStreamReader reader = new InputStreamReader(ins, charset);
            InputStreamReader reader = new InputStreamReader(new FileInputStream(SRC_XML_FILE_PATH), charset);
            int singleChar = 0;
            while ((singleChar = reader.read()) != -1) {
                System.out.println((char)singleChar);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
