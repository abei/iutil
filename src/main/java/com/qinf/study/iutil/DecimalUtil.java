package com.qinf.study.iutil;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by qinf on 2020-03-05.
 */
public class DecimalUtil {

    // Decimal scale(Decimal places).
    private static final int DEFAULT_SCALE = 10;
    private static final int DEFAULT_ROUND_MODE = BigDecimal.ROUND_HALF_UP;

    /**
     * Division of high-precision numbers. Scale as default is ten digits after decimal point,
     * subsequent numbers will be rounded by default round mode - ROUND_HALF_UP.
     *
     * @param divisor
     * @param dividend
     * @return
     */
    public static Double divide(Double divisor, Double dividend) {
        return divide(divisor, dividend, DEFAULT_SCALE);
    }

    /**
     * Division of high-precision numbers. Scale is specified by invoker,
     * subsequent numbers will be rounded by default round mode - ROUND_HALF_UP.
     *
     * @param divisor
     * @param dividend
     * @param scale
     * @return
     */
    public static Double divide(Double divisor, Double dividend, int scale) {
        Objects.requireNonNull(divisor, "Divisor cannot be null!");
        Objects.requireNonNull(dividend, "Dividend cannot be null!");
        return divide(divisor.toString(), dividend.toString(), scale, DEFAULT_ROUND_MODE).doubleValue();
    }

    /**
     * Division of high-precision numbers. Scale is specified by invoker,
     * subsequent numbers will be rounded default round mode - ROUND_HALF_UP,
     * and the result is text.
     *
     * @param divisor
     * @param dividend
     * @param scale
     * @return
     */
    public static String divide(String divisor, String dividend, int scale) {
        return divide(divisor, dividend, scale, DEFAULT_ROUND_MODE).toString();
    }

    /**
     * Division of high-precision numbers. Scale and round mode are both specified by invoker,
     * and the result is text.
     *
     * @param divisor
     * @param dividend
     * @param scale
     * @return
     */
    public static BigDecimal divide(String divisor, String dividend, int scale, int roundMode) {
        Objects.requireNonNull(divisor, "Divisor cannot be null!");
        Objects.requireNonNull(dividend, "Dividend cannot be null!");
        if (scale < 0) throw new IllegalArgumentException("Scale cannot be negative!");
        if (!RoundMode.isValidMode(roundMode)) throw new IllegalArgumentException("Round mode is invalid!");
        BigDecimal bigDivisor = new BigDecimal(divisor);
        BigDecimal bigDividend = new BigDecimal(dividend);
        return bigDivisor.divide(bigDividend, scale, roundMode);
    }

    /**
     * Multiplication of high-precision numbers.
     *
     * @param multiplier
     * @param multiplicand
     * @return
     */
    public static Double multiply(Double multiplier, Double multiplicand) {
        BigDecimal bigMultiplier = new BigDecimal(multiplier.toString());
        BigDecimal bigMultiplicand = new BigDecimal(multiplicand.toString());
        return multiply(multiplier, multiplicand, DEFAULT_SCALE);
    }

    public static Double multiply(Double multiplier, Double multiplicand, int scale) {
        Objects.requireNonNull(multiplier, "Multiplier cannot be null!");
        Objects.requireNonNull(multiplicand, "Multiplicand cannot be null!");
        return multiply(multiplier.toString(), multiplicand.toString(), scale, DEFAULT_ROUND_MODE).doubleValue();
    }

    public static String multiply(String multiplier, String multiplicand) {
        return multiply(multiplier, multiplicand, DEFAULT_SCALE);
    }

    public static String multiply(String multiplier, String multiplicand, int scale) {
        return multiply(multiplier, multiplicand, scale, DEFAULT_ROUND_MODE).toString();
    }

    public static BigDecimal multiply(String multiplier, String multiplicand, int scale, int roundMode) {
        Objects.requireNonNull(multiplier, "Multiplier cannot be null!");
        Objects.requireNonNull(multiplicand, "Multiplicand cannot be null!");
        if (scale < 0) throw new IllegalArgumentException("Scale cannot be negative!");
        if (!RoundMode.isValidMode(roundMode)) throw new IllegalArgumentException("Round mode is invalid!");
        BigDecimal bigMultiplier = new BigDecimal(multiplier);
        BigDecimal bigMultiplicand = new BigDecimal(multiplicand);
        return bigMultiplier.multiply(bigMultiplicand).setScale(scale, roundMode);
    }

    enum RoundMode {
        HALF_UP(BigDecimal.ROUND_HALF_UP),
        DOWN(BigDecimal.ROUND_DOWN);

        RoundMode(int mode) {
            this.mode = mode;
        }

        int mode;

        public static boolean isValidMode(int mode) {
            return Stream.of(HALF_UP, DOWN)
                    .anyMatch(m -> Objects.equals(m.mode, mode));
        }
    }
}
