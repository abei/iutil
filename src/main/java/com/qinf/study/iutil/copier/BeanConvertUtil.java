package com.qinf.study.iutil.copier;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.ClassUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.springframework.beans.BeanUtils.getPropertyDescriptor;
import static org.springframework.beans.BeanUtils.getPropertyDescriptors;

/**
 * The bean copier implemented by Spring bean util. If you want
 * use it, more third-party jars you need reference(spring-core & spring-beans).
 * <p>
 * Copying occurs need meet one condition that is source field value
 * is not null and target field value is null. And the copier is not
 * deep clone.
 *
 * Created by qinf on 2020-03-05.
 */
@Deprecated
public class BeanConvertUtil {

    @Deprecated
    public static <E, D> D convertIgnoreNullProperty(E source, D target) {
        if (Objects.isNull(source) || Objects.isNull(target)) {
            return target;
        }
        copyPropertiesIgnoreNullProperties(source, target, getNullPropertyNames(source));
        return target;
    }

    private static void copyPropertiesIgnoreNullProperties(Object source, Object target, String... ignoredPropertyNamesOfSource) {
        final BeanWrapper targetWrapper = new BeanWrapperImpl(target);
        Class<?> actualEditable = target.getClass();
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        List<String> ignoreList = (ignoredPropertyNamesOfSource != null ? Arrays.asList(ignoredPropertyNamesOfSource) : null);

        for (PropertyDescriptor targetPd : targetPds) {
            if (Objects.isNull(targetWrapper.getPropertyValue(targetPd.getName()))) {
                Method writeMethod = targetPd.getWriteMethod();
                if (writeMethod != null && (ignoreList == null || !ignoreList.contains(targetPd.getName()))) {
                    PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                    if (sourcePd != null) {
                        Method readMethod = sourcePd.getReadMethod();
                        if (readMethod != null &&
                                ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                            try {
                                if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                    readMethod.setAccessible(true);
                                }
                                Object value = readMethod.invoke(source);
                                if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                    writeMethod.setAccessible(true);
                                }
                                writeMethod.invoke(target, value);
                            } catch (Throwable ex) {
                                throw new RuntimeException("Cannot copy field value from source bean to target bean, target field name=" + targetPd.getName(), ex);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Get all field names from specified bean that the these field values are null.
     */
    private static String[] getNullPropertyNames(Object bean) {
        final BeanWrapper beanWrapper = new BeanWrapperImpl(bean);
        PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();
        String[] ignoredPropertyNames = Arrays.stream(propertyDescriptors)
                .filter(pd -> Objects.isNull(beanWrapper.getPropertyValue(pd.getName())))
                .map(PropertyDescriptor::getName)
                .distinct()
                .toArray(String[]::new);
        return ignoredPropertyNames;
    }
}
