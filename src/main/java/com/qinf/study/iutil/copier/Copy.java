package com.qinf.study.iutil.copier;

/**
 * Created by qinf on 2020-03-06.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Copy {
    String TO() default "";
    String FROM() default "";
}
