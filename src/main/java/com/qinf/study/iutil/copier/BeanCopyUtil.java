package com.qinf.study.iutil.copier;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.qinf.study.iutil.copier.BeanCopyUtil.ObjectType.*;

/**
 * Created by qinf on 2020-03-06.
 */
public class BeanCopyUtil {

    /**
     * The mode is say that only the field value of source bean is not null,
     * and the field value of target bean is null, then copy operation will
     * be executed, or will ignore copy behavior this time.
     */
    public static final int BEAN_TO_BEAN_IGNORE_NULL = 0;

    /**
     * No matter the field value from source bean and target bean is null or not,
     * the copy operation always to be executed.
     */
    public static final int BEAN_TO_BEAN_ALL_COVER = 1;

    /**
     * Copy the source map to target bean, the key of map will be converted
     * into camel case if it is snake case. It is default copying mode of map to bean.
     */
    public static final int MAP_TO_BEAN_CAMEL_CASE = 2;

    /**
     * Copy the source map to target bean ,the key of map and the field name of target
     * bean must be identical.
     */
    public static final int MAP_TO_BEAN = 3;

    private static final List<Integer> validCopyingModes = Arrays.asList(
            BEAN_TO_BEAN_IGNORE_NULL,
            BEAN_TO_BEAN_ALL_COVER,
            MAP_TO_BEAN,
            MAP_TO_BEAN_CAMEL_CASE
    );

    /**
     * The mapping of primitive to wrapper of eight class types.
     */
    private static Map<Class, Class> primitiveWrapperMapping = null;

    static {
        primitiveWrapperMapping = new HashMap<>();
        primitiveWrapperMapping.put(Short.TYPE, Short.class);
        primitiveWrapperMapping.put(Integer.TYPE, Integer.class);
        primitiveWrapperMapping.put(Long.TYPE, Long.class);
        primitiveWrapperMapping.put(Float.TYPE, Float.class);
        primitiveWrapperMapping.put(Double.TYPE, Double.class);
        primitiveWrapperMapping.put(Character.TYPE, Character.class);
        primitiveWrapperMapping.put(Byte.TYPE, Byte.class);
        primitiveWrapperMapping.put(Boolean.TYPE, Boolean.class);
    }

    private static NumberFormat longNumberFormat = null;

    private static NumberFormat doubleNumberFormat = null;

    static {
        longNumberFormat = NumberFormat.getInstance();
        longNumberFormat.setMaximumFractionDigits(10);
        longNumberFormat.setGroupingUsed(false);
        doubleNumberFormat = NumberFormat.getInstance();
        doubleNumberFormat.setMaximumFractionDigits(20);
        doubleNumberFormat.setGroupingUsed(false);
    }

    /**
     * Copy field values from source bean to target bean by
     * copying mode COPY_IGNORE_NULL_MODE. Support copy map to bean.
     *
     * @param source source bean object
     * @param target target bean object
     */
    public static <E, D> D copy(E source, D target) {
        if (Map.class.isAssignableFrom(source.getClass())) {
            copy(source, target, MAP_TO_BEAN_CAMEL_CASE);
            return target;
        }
        copy(source, target, BEAN_TO_BEAN_IGNORE_NULL);
        return target;
    }

    /**
     * Copy field values from source bean to target bean instantiated by target class by copying
     * mode COPY_IGNORE_NULL_MODE. Also Support copy map to bean by default MAP_TO_BEAN_CAMEL_CASE.
     *
     * @param source      source bean object
     * @param targetClass target bean class
     */
    public static <E, T> T copy(E source, Class<T> targetClass) {
        T target = null;
        try {
            try {
                targetClass.getConstructor(null);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException("The target class has not declared public and non-args constructor, but that is necessary for instantiating the class.");
            }
            target = targetClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        copy(source, target);
        return target;
    }

    /**
     * Copy field values from source bean to target bean by default
     * copying mode COPY_ALL_COVER_MODE.
     *
     * @param source source bean object
     * @param target target bean object
     */
    public static <E, D> D forceCopy(E source, D target) {
        copy(source, target, BEAN_TO_BEAN_ALL_COVER);
        return target;
    }

    /**
     * Copy field values from source bean to target bean by the
     * copying mode specified by invoker.
     *
     * @param source source bean object
     * @param target target bean object
     */
    public static <E, D> D copy(E source, D target, int copyMode) {
        if (!validCopyingModes.contains(copyMode)) {
            throw new IllegalArgumentException("Illegal copying mode.");
        }
        if (MAP_TO_BEAN == copyMode || MAP_TO_BEAN_CAMEL_CASE == copyMode) {
            if (Objects.isNull(source) || ((Map) source).isEmpty()
                    || Objects.isNull(target)) {
                return target;
            }
            doCopyMapToBean(source, target, copyMode);
            return target;
        }
        if (Objects.isNull(source) || Objects.isNull(target)) {
            return target;
        }
        Map<String, Object> vessel = new HashMap<>(16);
        pumpOutSource(source, source.getClass(), vessel);
        pumpInTarget(target, target.getClass(), vessel, copyMode);
        return target;
    }

    /**
     * Get field values from source bean object, and pump these values out to a
     * temporary cache. If the source bean have fields inherited from its super
     * class, then the field values from super class will also pumped out to
     * above cache by recursive approach.
     *
     * @param source      source bean object
     * @param sourceClass source bean class object or its super class object
     * @param vessel      cache the values in the vessel
     */
    private static void pumpOutSource(Object source, Class<?> sourceClass, Map<String, Object> vessel) {
        /** If no super class will end recursion */
        if (Objects.isNull(sourceClass.getSuperclass())) {
            return;
        }
        /**
         * allDeclaredFields includes public, protected, default(package) access,
         * and private fields, but excludes inherited fields.
         */
        Field[] allSourceFields = sourceClass.getDeclaredFields();
        Stream.of(allSourceFields).forEach(field -> {
            try {
                String fieldName = field.getName();
                Copy copyAnnotation = field.getAnnotation(Copy.class);
                if (Objects.nonNull(copyAnnotation)) {
                    fieldName = copyAnnotation.TO();
                }
                field.setAccessible(true);
                Object fieldValue = field.get(source);
                vessel.put(fieldName, fieldValue);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Pump value out to vessel failed, field name = " + field.getName(), e);
            }
        });
        pumpOutSource(source, sourceClass.getSuperclass(), vessel);
    }

    /**
     * Get field values from cached vessel, and pump these values into target bean object.
     * If the target bean have fields inherited from its super class, then will also try
     * to set these inherited fields by recursive way.
     *
     * @param target       target bean object
     * @param targetClass  target bean class object or its super class object
     * @param vessel       vessel contains the source field values
     * @param beanCopyMode bean copying mode
     */
    private static void pumpInTarget(Object target, Class<?> targetClass, Map<String, Object> vessel, int beanCopyMode) {
        if (Objects.isNull(targetClass.getSuperclass())) {
            return;
        }
        Field[] allTargetFields = targetClass.getDeclaredFields();
        Stream.of(allTargetFields)
                .filter(field -> !(Modifier.isStatic(field.getModifiers())
                        && Modifier.isFinal(field.getModifiers())))
                .forEach(field -> {
                    try {
                        field.setAccessible(true);
                        if (BEAN_TO_BEAN_IGNORE_NULL == beanCopyMode
                                && Objects.nonNull(field.get(target))) {
                            // Here, "return" means continue in loop
                            return;
                        }
                        field.set(target, getCompatibleValue(vessel, field));
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException("Pump value into target field failed, field name = " + field.getName(), e);
                    }
                });
        pumpInTarget(target, targetClass.getSuperclass(), vessel, beanCopyMode);
    }

    /**
     * Do copy map to a bean.
     *
     * @param source      map instance
     * @param bean        target bean
     * @param mapCopyMode map copying mode
     */
    private static void doCopyMapToBean(Object source, Object bean, int mapCopyMode) {
        Map<String, Object> map = (Map) source;
        Class beanClazz = bean.getClass();
        List<Class> beanAllClazz = new ArrayList<>(5);
        while (true) {
            beanAllClazz.add(beanClazz);
            beanClazz = beanClazz.getSuperclass();
            if (Objects.isNull(beanClazz) || beanClazz.isAssignableFrom(Object.class)) {
                break;
            }
        }
        List<Field> fields = beanAllClazz.stream()
                .flatMap(clz -> Stream.of(clz.getDeclaredFields()))
                .filter(field -> !Modifier.isStatic(field.getModifiers())
                        && !Modifier.isFinal(field.getModifiers()))
                .collect(Collectors.toList());
        // Do copy map to bean by MAP_TO_BEAN_CAMEL_CASE mode, the key is camel case
        if (MAP_TO_BEAN_CAMEL_CASE == mapCopyMode) {
            Map camelCaseMap = map.entrySet().stream()
                    .collect(HashMap<String, Object>::new,
                            (m, e) -> m.put(toCamelCase(e.getKey()), e.getValue()),
                            HashMap::putAll);
            fields.forEach(field -> {
                Copy copyAnnotation = field.getAnnotation(Copy.class);
                if (Objects.nonNull(copyAnnotation)) {
                    String annotatedFieldName = copyAnnotation.FROM();
                    if (map.containsKey(annotatedFieldName)) {
                        camelCaseMap.put(annotatedFieldName, map.get(annotatedFieldName));
                    }
                }
            });
            map.clear();
            fields.forEach(field -> {
                try {
                    if (!camelCaseMap.containsKey(getIdealTargetFieldName(field))) {
                        return;
                    }
                    field.setAccessible(true);
                    field.set(bean, getCompatibleValue(camelCaseMap, field));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
            return;
        }
        // Do copy map to bean by MAP_TO_BEAN mode, the key and the field name is sensitive case
        fields.forEach(field -> {
            if (!map.containsKey(getIdealTargetFieldName(field))) {
                return;
            }
            try {
                field.setAccessible(true);
                field.set(bean, getCompatibleValue(map, field));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    private static String getIdealTargetFieldName(Field field) {
        String fieldName = field.getName();
        Copy copyAnnotation = field.getAnnotation(Copy.class);
        if (Objects.nonNull(copyAnnotation)) {
            fieldName = copyAnnotation.FROM();
        }
        return fieldName;
    }

    private static String getAnnotatedDateFormat(Field field) {
        DateFormat dfAnnotation = field.getAnnotation(DateFormat.class);
        return Objects.nonNull(dfAnnotation) ? dfAnnotation.FORMAT() : "";
    }

    /**
     * Interactively convert class type of value from map and field of bean automatically.
     * This can copy the field from source to target even through the field data type of
     * these two fields is not exactly identical.
     * Currently, compatible copying of different field data type as following:<br>
     * String -> Number<br>
     * String -> Boolean<br>
     * String -> java.util.Date<br>
     * Boolean -> String<br>
     * Number -> String<br>
     * Long -> java.util.Date<br>
     * java.util.Date -> String<br>
     * java.util.Date -> Long
     *
     * @param map   the source values
     * @param filed the target field that value copied to
     * @return Object the converted compatible value
     */
    private static Object getCompatibleValue(Map map, Field filed) {
        Object value = map.get(getIdealTargetFieldName(filed));
        if (Objects.isNull(value)) {
            return value;
        }
        // As the value is from Map, so here no need consider it is primitive type or not
        Class valueType = value.getClass();
        // The field type may be primitive, so here must check and do necessary wrapping
        Class fieldType = toWrapClazz(filed.getType());
        if (fieldType.isAssignableFrom(valueType)) {
            return value;
        }
        if (STRING.clazz.isAssignableFrom(valueType)) {
            String wrappedStringValue = (String) value;
            if (NUMBER.clazz.isAssignableFrom(fieldType)) {
                if (!wrappedStringValue.chars().allMatch(Character::isDigit)) {
                    throw new ClassCastException("The value : " + wrappedStringValue + " cannot cast to number.");
                }
                switch (ObjectType.getObjectType(fieldType)) {
                    case SHORT:
                        value = Short.valueOf(wrappedStringValue);
                        break;
                    case INTEGER:
                        value = Integer.valueOf(wrappedStringValue);
                        break;
                    case LONG:
                        value = Long.valueOf(wrappedStringValue);
                        break;
                    case FLOAT:
                        value = Float.valueOf(wrappedStringValue);
                        break;
                    case DOUBLE:
                        value = Double.valueOf(wrappedStringValue);
                        break;
                    case BYTE:
                        value = Byte.valueOf(wrappedStringValue);
                        break;
                    case BIG_DECIMAL:
                        value = new BigDecimal(wrappedStringValue);
                        break;
                    case BIG_INTEGER:
                        value = new BigInteger(wrappedStringValue);
                        break;
                    case ATOMIC_INTEGER:
                        value = new AtomicInteger(Integer.parseInt(wrappedStringValue));
                        break;
                    case ATOMIC_LONG:
                        value = new AtomicLong(Long.parseLong(wrappedStringValue));
                        break;
                    default:
                        break;
                }
            } else if (BOOLEAN.clazz.isAssignableFrom(fieldType)) {
                value = Boolean.valueOf(wrappedStringValue.toLowerCase());
            } else if (UTIL_DATE.clazz.isAssignableFrom(fieldType)) {
                String dateFormat = getAnnotatedDateFormat(filed);
                if (dateFormat.isEmpty()) {
                    dateFormat = wrappedStringValue.split(" ").length == 2
                            ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd";
                }
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                try {
                    value = sdf.parse(wrappedStringValue);
                } catch (ParseException e) {
                    throw new NumberFormatException(e.getMessage());
                }
            }
        } else if (BOOLEAN.clazz.isAssignableFrom(valueType)) {
            if (STRING.clazz.isAssignableFrom(fieldType)) {
                value = String.valueOf(value);
            } else {
                throw new ClassCastException("The value is boolean type, cannot cast to " + fieldType.getName() + ".");
            }
        } else if (NUMBER.clazz.isAssignableFrom(valueType)) {
            if (STRING.clazz.isAssignableFrom(fieldType)) {
                switch (ObjectType.getObjectType(valueType)) {
                    case FLOAT:
                        Float wrappedFloatValue = (Float) value;
                        value = longNumberFormat.format(wrappedFloatValue);
                        break;
                    case DOUBLE:
                        Double wrappedDoubleValue = (Double) value;
                        value = doubleNumberFormat.format(wrappedDoubleValue);
                        break;
                    default:
                        value = String.valueOf(value);
                        break;
                }
            } else if (UTIL_DATE.clazz.isAssignableFrom(fieldType)) {
                if (Objects.equals(LONG, ObjectType.getObjectType(valueType))) {
                    Long wrappedLongValue = (Long) value;
                    if (0 == wrappedLongValue) {
                        value = null;
                        return value;
                    }
                    int digitNumber = String.valueOf(wrappedLongValue).length();
                    if (10 == digitNumber) {
                        value = new Date(wrappedLongValue * 1000);
                    } else if (13 == digitNumber) {
                        value = new Date(wrappedLongValue);
                    } else {
                        throw new UnsupportedOperationException("Currently just support 10 or 13 digits number cast to " + fieldType.getName() + ", but current number is " + digitNumber + " digits.");
                    }
                } else {
                    throw new UnsupportedOperationException("The value is " + value.getClass().getTypeName() + " type, currently not support cast to " + fieldType.getName() + ".");
                }
            } else {
                throw new UnsupportedOperationException("The value is " + value.getClass().getTypeName() + " type, currently not support cast to " + fieldType.getName() + ".");
            }
        } else if (UTIL_DATE.clazz.isAssignableFrom(valueType)) {
            Date wrappedDateValue = (Date) value;
            if (LONG.clazz.isAssignableFrom(fieldType)) {
                value = wrappedDateValue.getTime();
            } else if (STRING.clazz.isAssignableFrom(fieldType)) {
                String dateFormat = getAnnotatedDateFormat(filed);
                if (dateFormat.isEmpty()) {
                    dateFormat = "yyyy-MM-dd HH:mm:ss";
                }
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                value = sdf.format(wrappedDateValue);
            } else {
                throw new ClassCastException("The value is java.util.Date type, cannot cast to " + fieldType.getName() + ".");
            }
        }
        return value;
    }

    /**
     * Convert string in snake case format to camel case. Can see a case,
     * "hello_world" to "helloWorld". Further, the method only convert the first
     * letter closest to underline "_" from the right to upper case, the other letters
     * keep the same, that is say "hello_worLd" to "helloWorLd".
     *
     * @param snakeCase text in snake case
     * @return String text in converted camel case
     */
    public static String toCamelCase(String snakeCase) {
        Objects.requireNonNull(snakeCase, "The string for converting to camel case cannot be null.");
        if (snakeCase.isEmpty() || !snakeCase.contains("_")) {
            return snakeCase;
        }
        String[] splitSnakeCases = snakeCase.split("_");
        StringBuilder camelCase = new StringBuilder();
        camelCase.append(splitSnakeCases[0]);
        for (int i = 1; i < splitSnakeCases.length; i++) {
            camelCase.append(Character.toUpperCase(splitSnakeCases[i].charAt(0)))
                    .append(splitSnakeCases[i].substring(1));
        }
        return camelCase.toString();
    }

    /**
     * Wrap primitive value to boxed object.
     *
     * @param valueClazz primitive class
     * @return wrapped object with the primitive value
     */
    public static Class toWrapClazz(Class valueClazz) {
        if (valueClazz == Void.class || valueClazz == void.class) {
            return Void.class;
        }
        if (!valueClazz.isPrimitive()) {
            return valueClazz;
        }
        return primitiveWrapperMapping.get(valueClazz);
    }

    enum ObjectType {
        NUMBER(Number.class),
        SHORT(Short.class),
        INTEGER(Integer.class),
        LONG(Long.class),
        FLOAT(Float.class),
        DOUBLE(Double.class),
        BYTE(Byte.class),
        BIG_DECIMAL(BigDecimal.class),
        BIG_INTEGER(BigInteger.class),
        ATOMIC_INTEGER(AtomicInteger.class),
        ATOMIC_LONG(AtomicLong.class),
        BOOLEAN(Boolean.class),
        UTIL_DATE(Date.class),
        SQL_DATE(java.sql.Date.class),
        STRING(String.class);

        private Class clazz;
        private static final ObjectType[] CACHED_OBJECT_TYPES = ObjectType.values();

        ObjectType(Class clazz) {
            this.clazz = clazz;
        }

        protected static ObjectType getObjectType(Class clazz) {
            for (ObjectType objectType : CACHED_OBJECT_TYPES) {
                if (Objects.equals(objectType.clazz, toWrapClazz(clazz))) {
                    return objectType;
                }
            }
            return null;
        }
    }
}
