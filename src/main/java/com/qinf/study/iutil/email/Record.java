package com.qinf.study.iutil.email;

import lombok.Builder;
import lombok.Data;

/**
 * Created by qinf on 2020-12-14.
 */
@Data
public class Record {

    private String receiverEmailAddr;

    private String sendContent;

    private String isSucc;

    private String errorMsg = "";

    public static String SUCC = "Successful to send";

    public static String FAIL = "Failed to send";

    public String joinedContent() {
        return this.errorMsg.isEmpty() ? this.getIsSucc() + ": " + this.getReceiverEmailAddr() + "\t" + this.getSendContent()
                : this.getIsSucc() + ": " + this.getReceiverEmailAddr() + "\t" + errorMsg;
    }
}
