package com.qinf.study.iutil.email;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by qinf on 2020-12-13.
 */
public class EmailUtil {

    private static final Sender sender;

    private static final Session emailSession;

    /**
     * Init send service config.
     */
    static {
        sender = initSender();
        emailSession = initEmailSession();
    }

    private static Sender initSender() {
        String senderPath = System.getProperty("user.dir") + "\\sender.txt";
        Sender sender = new Sender();
        try (Stream<String> stream = Files.lines(Paths.get(senderPath))) {
            stream.filter(Objects::nonNull).forEach(line -> {
                String[] mapping = Stream.of(line.split(":")).map(String::trim).toArray(String[]::new);
                if (Objects.equals(mapping[0], "email_addr")) {
                    sender.setEmailAddr(mapping[1]);
                } else if (Objects.equals(mapping[0], "email_pwd")) {
                    sender.setEmailPwd(mapping[1]);
                } else if (Objects.equals(mapping[0], "email_server_protocol")) {
                    sender.setEmailServerProtocol(mapping[1]);
                } else if (Objects.equals(mapping[0], "email_server_host")) {
                    sender.setEmailServerHost(mapping[1]);
                } else if (Objects.equals(mapping[0], "email_server_port")) {
                    sender.setEmailServerPort(Integer.parseInt(mapping[1]));
                }
            });
        } catch (Exception e) {
            LogUtil.log("Error incurred when initialize sender configuration, please check sender.txt file!\n" + "Error message: " + e.getMessage());
            throw new RuntimeException(e);
        }
        return sender;
    }

    private static Session initEmailSession() {
        Session session;
        try {
            Properties serverConfOfSender = new Properties();
            serverConfOfSender.put("mail.store.protocol", sender.getEmailServerProtocol());
            serverConfOfSender.put("mail.smtp.host", sender.getEmailServerHost());
            serverConfOfSender.put("mail.smtp.port", sender.getEmailServerPort());
            serverConfOfSender.put("mail.smtp.auth", true);
            serverConfOfSender.put("mail.smtp.starttls.enable", "true");
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(sender.getEmailAddr(), sender.getEmailPwd());
                }
            };
            session = Session.getInstance(serverConfOfSender, authenticator);
            session.setDebug(false);
        } catch (Exception e) {
            LogUtil.log("Error incurred when initialize email session configuration, please check sender.txt file!\n" + "Error message: " + e.getMessage());
            throw new RuntimeException(e);
        }
        return session;
    }

    private static String getCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case _NONE:
                break;
            case NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case FORMULA:
                break;
            case BLANK:
                break;
            case ERROR:
                break;
        }
        return "";
    }

    private static Set<Receiver> getReceivers() {
        String receiverPath = System.getProperty("user.dir") + "\\receivers.xlsx";
        Set<Receiver> receivers;
        try (FileInputStream inputStream = new FileInputStream(new File(receiverPath))) {
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheetAt(0);
            int rowSum = sheet.getLastRowNum() + 1;
            if (rowSum <= 2) {
                throw new RuntimeException("The content of receivers.xlsx is invalid, no employee info was added into the file.");
            }
            receivers = new HashSet<>();
            List<String> cellNames = new ArrayList<>();
            String emailSubject = "";
            for (int i = 0; i < rowSum; i++) {
                // Get email subject.
                if (i == 0) {
                    Iterator<Cell> cellIterator = sheet.getRow(i).cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        String cellValue = getCellValue(cell);
                        if (Objects.nonNull(cellValue) && !cellValue.isEmpty()) {
                            emailSubject = cellValue;
                            break;
                        }
                    }
                } else if (i == 1) {
                    // Get item name of a row.
                    Iterator<Cell> cellIterator = sheet.getRow(i).cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        String cellValue = getCellValue(cell);
                        cellNames.add(cellValue);
                    }
                } else {
                    // Get concrete receiver info.
                    Row row = sheet.getRow(i);
                    int cellSum = row.getLastCellNum();
                    Receiver receiver = new Receiver();
                    for (int j = 0; j < cellSum; j++) {
                        String maybeEmail = getCellValue(row.getCell(j));
                        if (maybeEmail.contains("@")) {
                            receiver.setEmailAddr(maybeEmail);
                        }
                        receiver.getEmailMapping().put(cellNames.get(j), getCellValue(row.getCell(j)));
                    }
                    receiver.setEmailSubject(emailSubject);
                    receivers.add(receiver);
                }
            }
        } catch (Exception e) {
            LogUtil.log("Error incurred when get receivers, please check receivers.xlsx file!\n" + "Error message: " + e.getMessage());
            throw new RuntimeException(e);
        }
        return receivers;
    }

    private static Multipart emailCtn(Receiver receiver) throws MessagingException {
        BodyPart contentPart = new MimeBodyPart();
        contentPart.setContent(Optional.ofNullable(receiver.emailFormattedContent()).orElse(""), "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(contentPart);
        return multipart;
    }

    private static Record doSend(final Receiver receiver) {
        Record record = new Record();
        record.setReceiverEmailAddr(receiver.getEmailAddr());
        record.setSendContent(receiver.toString());
        try {
            MimeMessage message = new MimeMessage(emailSession);
            message.setFrom(new InternetAddress(sender.getEmailAddr()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver.getEmailAddr()));
            message.setSubject(Optional.ofNullable(receiver.getEmailSubject()).orElse(""));
            message.setSentDate(new Date());
            message.setContent(emailCtn(receiver));
            message.saveChanges();
            Transport.send(message);
        } catch (Exception e) {
            record.setIsSucc(Record.FAIL);
            record.setErrorMsg(e.getMessage());
        }
        record.setIsSucc(Record.SUCC);
        return record;
    }

    public static void sendEmail() {
        Set<Receiver> receivers = getReceivers();
        boolean failed = false;
        for (Receiver receiver : receivers) {
            Record record = doSend(receiver);
            LogUtil.log(record.joinedContent());
            if (Objects.equals(record.getIsSucc(), Record.FAIL)) {
                failed = true;
            }
        }
        /*receivers.forEach(receiver -> {
            Record record = doSend(receiver);
            LogUtil.log(record.joinedContent());
            if (Objects.equals(record.getIsSucc(), Record.FAIL)) {
                failed = true;
            }
        });*/
        if (failed) {
            JOptionPane.showMessageDialog(null, "Some email fail to send, you can get more detail from log file!", "",JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "All emails successful to send!", "", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
