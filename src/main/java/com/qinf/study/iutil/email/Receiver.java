package com.qinf.study.iutil.email;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qinf on 2020-12-13.
 */
@Data
public class Receiver {

    private String emailSubject;

    private String emailAddr;

    private Map<String, String> emailMapping = new HashMap<>();

    public String emailFormattedContent() {
        StringBuilder content = new StringBuilder("<html><body>");
        content.append("<table border=\"1\">");
        content.append("<tr>");
        this.emailMapping.entrySet().forEach(entry -> {
            content.append("<th>" + entry.getKey() + "</th>");
        });
        content.append("</tr>");
        content.append("<tr>");
        this.emailMapping.entrySet().forEach(entry -> {
            content.append("<th>" + entry.getValue() + "</th>");
        });
        content.append("</tr>");
        content.append("</table>");
        content.append("</body></html>");
        return content.toString();
    }
}
