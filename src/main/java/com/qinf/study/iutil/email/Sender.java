package com.qinf.study.iutil.email;

import lombok.Data;

/**
 * Created by qinf on 2020-12-13.
 */
@Data
public class Sender {

    private String emailAddr;

    private String emailPwd;

    private String emailServerProtocol;

    private String emailServerHost;

    private int emailServerPort;
}
