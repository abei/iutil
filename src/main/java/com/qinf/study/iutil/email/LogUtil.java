package com.qinf.study.iutil.email;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Created by qinf on 2020-12-14.
 */
public class LogUtil {

    private static Path logFilePath;

    static {
        String logFileSuffix = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();
        logFilePath = Paths.get(System.getProperty("user.dir") + "\\send_" + logFileSuffix + ".log");
    }

    public static void log(String log) {
        try {
            if (!Files.exists(logFilePath)) {
                Files.createFile(logFilePath);
            }
            Files.write(logFilePath, Optional.ofNullable(log).orElse("").concat("\n").getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
