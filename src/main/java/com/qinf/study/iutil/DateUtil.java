package com.qinf.study.iutil;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * Created by qinf on 2020-03-05.
 */
public class DateUtil {

    public static final String DATE_FORMAT_STRIKE_10 = "yyyy-MM-dd";

    public static final String DATE_FORMAT_LEFTSLASH_10 = "yyyy/MM/dd";

    public static final String DATE_FORMAT_19 = "yyyy-MM-dd HH:mm:ss";

    public static boolean isSameDay(Date date1, Date date2) {
        if (Objects.isNull(date1) && Objects.isNull(date2)) return true;
        if (Objects.isNull(date1) || Objects.isNull(date2)) return false;
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (Objects.isNull(cal1) && Objects.isNull(cal2)) return true;
        if (Objects.isNull(cal1) || Objects.isNull(cal2)) return false;
        return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    public static boolean isToday(Calendar cal) {
        return isSameDay(cal, Calendar.getInstance());
    }

    public static boolean isCurrentMonth(Date date) {
        if (Objects.isNull(date)) return false;
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date());
        return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }

    public static boolean isSameMonth(Date date1, Date date2) {
        if (Objects.isNull(date1) && Objects.isNull(date2)) return true;
        if (Objects.isNull(date1) || Objects.isNull(date2)) return false;
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }

    public static Date getFirstDayOfCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getFirstDayOfLastMonth() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getFirstDayOfMonth(Date date) {
        if (Objects.isNull(date)) return null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Get date(time) of the specified hours ago.
     *
     * @param currentDate current date
     * @param hours       specified hours ago
     */
    public static Date beforeHoursOfDate(Date currentDate, int hours) {
        if (Objects.isNull(currentDate) || hours == 0) return currentDate;
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.HOUR_OF_DAY, hours < 0 ? hours : -hours);
        return cal.getTime();
    }

    /**
     * Get date(time) of the specified hours later.
     *
     * @param currentDate current date
     * @param hours       specified hours later
     */
    public static Date afterHoursOfDate(Date currentDate, int hours) {
        if (Objects.isNull(currentDate) || hours == 0) return currentDate;
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.HOUR_OF_DAY, hours < 0 ? -hours : hours);
        return cal.getTime();
    }

    /**
     * Get date(time) of the specified a few days ago.
     *
     * @param date specified date
     * @param days a few days ago
     */
    public static Date beforeDaysOfDate(Date date, int days) {
        if (Objects.isNull(date) || days == 0) return date;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayOfDate = cal.get(Calendar.DATE);
        cal.set(Calendar.DATE, dayOfDate - (days < 0 ? -days : days));
        return cal.getTime();
    }

    /**
     * Get date(time) of the specified a few days later.
     *
     * @param date specified date
     * @param days a few days later
     */
    public static Date afterDaysOfDate(Date date, int days) {
        if (Objects.isNull(date) || days == 0) return date;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayOfDate = cal.get(Calendar.DATE);
        cal.set(Calendar.DATE, dayOfDate + (days < 0 ? -days : days));
        return cal.getTime();
    }

    public static Date getCurrentEarliestTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getEarliestTime(Date date) {
        if (Objects.isNull(date)) return null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getLatestTime(Date date) {
        if (Objects.isNull(date)) return null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static String getYMDOfDate(Date date) {
        if (Objects.isNull(date)) return "";
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate.toString();
    }

    public static Date string2Date(String dateTxt) {
        if (Objects.isNull(dateTxt) || dateTxt.isEmpty()) return null;
        SimpleDateFormat sdf = dateTxt.split(" ").length == 2 ?
                new SimpleDateFormat(DATE_FORMAT_19) :
                new SimpleDateFormat(DATE_FORMAT_STRIKE_10);
        Date date = null;
        try {
            date = sdf.parse(dateTxt);
        } catch (ParseException e) {
            throw new NumberFormatException(e.getMessage());
        }
        return date;
    }

    public static Date string2Date(String dateTxt, String dateFormat) {
        if (Objects.isNull(dateTxt) || dateTxt.isEmpty() || Objects.isNull(dateFormat) || dateFormat.isEmpty())
            return null;
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date date;
        try {
            date = sdf.parse(dateTxt);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return date;
    }

    public static Timestamp string2Timestamp(String dateTxt, String dateFormat) {
        Date date = string2Date(dateTxt, dateFormat);
        Objects.requireNonNull(date);
        return new Timestamp(date.getTime());
    }

    public static String date2String(Date date) {
        if (Objects.isNull(date)) return null;
        return date2String(date, DATE_FORMAT_19);
    }

    public static String date2String(Date date, String dateFormat) {
        if (Objects.isNull(date) || Objects.isNull(dateFormat) || dateFormat.isEmpty())
            return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            return sdf.format(date);
        } catch (Exception e) {
            throw e;
        }
    }

    public static String timestamp2String(Timestamp timestamp, String dateFormat) {
        Objects.requireNonNull(timestamp);
        Objects.requireNonNull(dateFormat);
        return date2String(timestamp, dateFormat);
    }

    public static String timestamp2String(Timestamp timestamp) {
        return timestamp2String(timestamp, DATE_FORMAT_19);
    }

    public static boolean isValidDate(String dateTxt, String dateFormat) {
        try {
            if (Objects.isNull(dateFormat) || dateFormat.isEmpty()) {
                string2Date(dateTxt);
            } else {
                string2Date(dateTxt, dateFormat);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String secToHMSFormat(int durInSec) {
        StringBuilder formattedTime = new StringBuilder();
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (durInSec <= 0) {
            return formattedTime
                    .append("00:00:00")
                    .toString();
        } else {
            minute = durInSec / 60;
        }
        if (minute < 1) {
            second = durInSec;
            return formattedTime.append("00:00:")
                    .append(unitFormat(second))
                    .toString();
        } else if (minute < 60) {
            second = durInSec % 60;
            return formattedTime.append("00:")
                    .append(unitFormat(minute))
                    .append(":")
                    .append(unitFormat(second))
                    .toString();
        } else { // the duration >= 3600 seconds
            hour = minute / 60;
            minute = minute % 60;
            second = durInSec - hour * 3600 - minute * 60;
            return formattedTime.append(unitFormat(hour))
                    .append(":")
                    .append(unitFormat(minute))
                    .append(":")
                    .append(unitFormat(second))
                    .toString();
        }
    }

    private static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + i;
        else
            retStr = "" + i;
        return retStr;
    }

    public static long diffSeconds(Date currDate, Date preDate) {
        if (Objects.isNull(currDate) || Objects.isNull(preDate))
            throw new NullPointerException("The recent date cannot be null!");
        return (currDate.getTime() - preDate.getTime()) / 1000;
    }

    /**
     * Calculate the diff days of two dates.
     * Note: even diff time less than 24 hours between two different dates is considered a day.
     *
     * @param recDate recent date
     * @param preDate previous date
     * @return
     */
    public static int diffDays(Date recDate, Date preDate) {
        if (Objects.isNull(recDate) || Objects.isNull(preDate))
            throw new NullPointerException("The recent date cannot be null!");
        LocalDate recLdt = recDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate preLdt = preDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        long diffDays = ChronoUnit.DAYS.between(recLdt, preLdt);
        return (int) (diffDays < 0 ? -diffDays : diffDays);
    }

    /**
     * Calculate the diff days of two dates by the diff milliseconds of the two dates.
     *
     * @param recDate recent date
     * @param preDate previous date
     * @return
     */
    public static long diffDaysByMis(Date recDate, Date preDate) {
        if (Objects.isNull(recDate) || Objects.isNull(preDate))
            throw new NullPointerException("The recent date cannot be null!");
        LocalDateTime recLdt = recDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime preLdt = preDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        Duration duration = Duration.between(recLdt, preLdt).abs();
        return duration.toDays();
    }

    public static boolean isHitTimeRange(Date startTime, Date hittingTime, Date endTime) {
        if (Objects.isNull(startTime) || Objects.isNull(hittingTime)
                || Objects.isNull(endTime)) throw new NullPointerException("Date cannot be null!");
        if (startTime.after(endTime)) throw new IllegalArgumentException("The range of date is illegal!");
        return hittingTime.after(startTime) && hittingTime.before(endTime);
    }
}
