package com.qinf.study.iutil;

import java.util.regex.Pattern;

/**
 * Created by qinf on 2020-03-05.
 */
public class PhoneUtil {

    private static final String MOBILE_PHONE_REG_EXPR = "^1[0-9]{10}$";
    private static final String FIXED_PHONE_REG_EXPR = "^(0[0-9]{2,3})?(\\-)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?$";
    private static final Pattern MOBILE_PHONE_PATTERN = Pattern.compile(MOBILE_PHONE_REG_EXPR);
    private static final Pattern FIXED_PHONE_PATTERN = Pattern.compile(FIXED_PHONE_REG_EXPR);

    /**
     * Determine whether it is a mobile phone number.
     *
     * @param phoneNum phone number
     */
    public static boolean isMobilePhone(String phoneNum) {
        return MOBILE_PHONE_PATTERN.matcher(phoneNum).find();
    }

    /**
     * Determine whether it is a fixed phone number.
     *
     * @param phoneNum phone number
     */
    public static boolean isFixedPhone(String phoneNum) {
        return FIXED_PHONE_PATTERN.matcher(phoneNum).find();
    }

    /**
     * Get pattern for judge mobile phone number.
     */
    public static Pattern getMobilePhonePattern() {
        return MOBILE_PHONE_PATTERN;
    }

    /**
     * Get pattern for judge fixed phone number.
     */
    public static Pattern getFixedPhonePattern() {
        return FIXED_PHONE_PATTERN;
    }
}
