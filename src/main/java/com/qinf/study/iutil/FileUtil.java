package com.qinf.study.iutil;

import lombok.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by qinf on 2021-07-26.
 */
public class FileUtil {

    public enum FileType {
        JPG("jpg", "FFD8FF", "JPEG(jpg)"),
        PNG("png", "89504E47", "PNG (png)"),
        GIF("gif", "47494638", "GIF (gif)"),
        TIF("tif", "49492A00", "TIFF (tif)"),
        BMP("bmp", "424D", "Windows Bitmap (bmp)"),
        DWG("dwg", "41433130", "CAD (dwg)"),
        HTML("html", "68746D6C3E", "HTML (html)"),
        RTF("rtf", "7B5C727466", "Rich Text Format (rtf)"),
        XML("xml", "3C3F786D6C", "Rich Text Format (rtf)"),
        ZIP("zip", "504B0304", ""),
        RAR("rar", "52617221", ""),
        PSD("psd", "38425053", "Photoshop (psd)"),
        EML("eml", "44656C69766572792D646174653A", "Email [thorough only] (eml)"),
        DBX("dbx", "CFAD12FEC5FD746F", "Outlook Express (dbx)"),
        PST("pst", "2142444E", "Outlook (pst)"),
        XLS("xls", "D0CF11E0", "MS Excel, the file header of word and excel is identical"),
        DOC("doc", "D0CF11E0", "MS Word, the file header of word and excel is identical"),
        MDB("mdb", "5374616E64617264204A", "MS Access (mdb)"),
        WPD("wpd", "FF575043", "WordPerfect (wpd)"),
        EPS("eps", "252150532D41646F6265", ""),
        PS("ps", "252150532D41646F6265", ""),
        PDF("pdf", "255044462D312E", "Adobe Acrobat (pdf)"),
        QDF("qdf", "AC9EBD8F", "Quicken (qdf)"),
        PWL("pwl", "E3828596", "Windows Password (pwl)"),
        WAV("wav", "57415645", "Wave (wav)"),
        AVI("avi", "41564920", ""),
        RAM("ram", "2E7261FD", "Real Audio (ram)"),
        RM("rm", "2E524D46", "Real Media (rm)"),
        MPG("mpg", "000001BA", ""),
        MOV("mov", "6D6F6F76", "Quicktime (mov)"),
        ASF("asf", "3026B2758E66CF11", "Windows Media (asf)"),
        MID("mid", "4D546864", "MIDI (mid)");

        private static final FileType[] CACHED_FILE_TYPES = FileType.values();

        @Getter
        private String code;

        @Getter
        private String magic;

        @Getter
        private String desc;

        FileType(String code, String magic, String desc) {
            this.code = code;
            this.magic = magic;
            this.desc = desc;
        }

        public static FileType getTypeByCode(String code) {
            if (Objects.isNull(code) || code.isEmpty()) {
                return null;
            }
            for (FileType type : CACHED_FILE_TYPES) {
                if (type.code.equalsIgnoreCase(code)) {
                    return type;
                }
            }
            return null;
        }
    }

    private final static Map<String, String> FILE_TYPE_MAPPING = Stream.of(FileType.CACHED_FILE_TYPES)
            .collect(Collectors.toMap(FileType::getCode, FileType::getMagic));

    /**
     * The InputStream can be read again after the method invoked.
     */
    public static String getFileTypeByIns(InputStream ins) {
        if (!ins.markSupported()) {
            throw new UnsupportedOperationException("Input stream not support mark and reset, you can use @getFileTypeByBytes() method.");
        }
        byte[] bytes = new byte[3072];
        try {
            ins.mark(0);
            ins.read(bytes);
            return getFileTypeByBytes(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                ins.reset();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static String getFileTypeByBytes(byte[] bytes) {
        String likelyMagicNumber = bytesToHex(Arrays.copyOf(bytes, 1024)).toUpperCase();
        return FILE_TYPE_MAPPING.entrySet().stream()
                .filter(entry -> likelyMagicNumber.startsWith(entry.getValue()))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(null);
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder bytesInHex = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                bytesInHex.append(0);
            }
            bytesInHex.append(hv);
        }
        return bytesInHex.toString();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InnerFile implements Serializable {
        private static final long serialVersionUID = 886589730;
        private FileType fileType;
        private String fileName;
        private byte[] fileContent;

        @Override
        public String toString() {
            return new StringJoiner(", ", this.getClass().getSimpleName() + "(", ")")
                    .add("fileType=" + fileType)
                    .add("fileName=" + fileName)
                    .add("fileContentBytesSize=" + Optional.ofNullable(fileContent).map(arr -> arr.length).orElse(0))
                    .toString();
        }
    }

    public static void zipMultiFiles(String compressedFileName, List<InnerFile> multiFiles) {
        if (Objects.isNull(compressedFileName) || compressedFileName.isEmpty()) {
            throw new IllegalArgumentException("Compressed file name must be specified!");
        }
        if (Objects.isNull(multiFiles) || multiFiles.isEmpty()) {
            throw new IllegalArgumentException("Files to be compressed is empty!");
        }
        try (FileOutputStream fos = new FileOutputStream(compressedFileName);
             ZipOutputStream zos = new ZipOutputStream(fos)) {
            writeToZip(zos, multiFiles);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] zipMultiFiles(List<InnerFile> multiFiles) {
        if (Objects.isNull(multiFiles) || multiFiles.isEmpty()) {
            return null;
        }
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ZipOutputStream zos = new ZipOutputStream(baos)) {
            writeToZip(zos, multiFiles);
            return baos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeToZip(ZipOutputStream zos, List<InnerFile> multiFiles) throws IOException {
        for (InnerFile innerFile : multiFiles) {
            if (0 == innerFile.getFileContent().length) {
                throw new RuntimeException("The content of file " + innerFile.getFileName() + " is empty");
            }
            ZipEntry zipEntry = new ZipEntry(innerFile.getFileName());
            zipEntry.setSize(innerFile.getFileContent().length);
            zos.putNextEntry(zipEntry);
            zos.write(innerFile.getFileContent());
        }
        zos.closeEntry();
    }

    public static byte[] unzipFile(InnerFile innerFile) {
        if (Objects.isNull(innerFile) || Objects.isNull(innerFile.getFileContent())) {
            return null;
        }
        byte[] zippedBytes = innerFile.getFileContent();
        if (0 == zippedBytes.length) {
            return new byte[0];
        }
        String fileType = getFileTypeByBytes(zippedBytes);
        if (!Objects.equals(FileType.ZIP.getCode(), fileType)) {
            throw new IllegalArgumentException("The file type is " + fileType + " but not zip!");
        }
        byte[] unzippedBytes = new byte[0];
        try (ByteArrayInputStream bais = new ByteArrayInputStream(zippedBytes);
             ZipInputStream zis = new ZipInputStream(bais, Charset.forName("GBK"))) {
            while (Objects.nonNull(zis.getNextEntry())) {
                byte[] buffer = new byte[1024];
                int length = -1;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                while ((length = zis.read(buffer, 0, buffer.length)) != -1) {
                    baos.write(buffer, 0, length);
                }
                unzippedBytes = baos.toByteArray();
                baos.flush();
                baos.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return unzippedBytes;
    }

    public static List<InnerFile> splitZippedMultiFilesBytes(byte[] multiFilesBytes) {
        if (Objects.isNull(multiFilesBytes) || 0 == multiFilesBytes.length) {
            return Collections.EMPTY_LIST;
        }
        List<InnerFile> innerFiles = new ArrayList<>(5);
        ZipEntry zipEntry;
        try (ByteArrayInputStream bais = new ByteArrayInputStream(multiFilesBytes);
             ZipInputStream zis = new ZipInputStream(bais, Charset.forName("GBK"))) {
            while (Objects.nonNull((zipEntry = zis.getNextEntry()))) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                final byte[] buffer = new byte[1024];
                int length;
                while ((length = zis.read(buffer, 0, buffer.length)) >= 0) {
                    baos.write(buffer, 0, length);
                }
                byte[] fileContent = baos.toByteArray();
                InnerFile innerFile = InnerFile.builder()
                        .fileContent(fileContent)
                        .fileName(zipEntry.getName())
                        .fileType(FileType.getTypeByCode(getFileTypeByBytes(fileContent)))
                        .build();
                innerFiles.add(innerFile);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return innerFiles;
    }

    public static Map<String, Object> parseXmlBytes(byte[] xmlBytes) {
        SAXReader saxReader = new SAXReader();
        Map<String, Object> parsedResult = new HashMap<>();
        try {
            Document document = saxReader.read(new ByteArrayInputStream(xmlBytes));
            Element rootElement = document.getRootElement();
            List<Element> allSubElements = rootElement.elements();
            parsedResult = readElement(allSubElements, parsedResult);
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
        return parsedResult;
    }

    private static Map<String, Object> readElement(List<Element> elements, Map<String, Object> parsedResult) {
        elements.forEach(element -> {
            if (element.hasMixedContent()) {
                if (parsedResult.keySet().contains(element.getName())) {
                    Map<String, Object> map = new HashMap<>();
                    List<Map<String, Object>> list = (List<Map<String, Object>>) parsedResult.get(element.getName());
                    list.add(readElement(element.elements(), map));
                    parsedResult.replace(element.getName(), list);
                } else {
                    Map<String, Object> map = new HashMap<>();
                    List<Map<String, Object>> list = new ArrayList<>();
                    list.add(readElement(element.elements(), map));
                    parsedResult.put(element.getName(), list);
                }
            } else {
                if (parsedResult.keySet().contains(element.getName())) {
                    Object object = parsedResult.get(element.getName());
                    List<Object> list = new ArrayList<>();
                    if (List.class.isAssignableFrom(object.getClass())) {
                        list = (List<Object>) object;
                    } else {
                        list.add(object);
                    }
                    list.add(element.getTextTrim());
                    parsedResult.replace(element.getName(), list);
                } else {
                    parsedResult.put(element.getName(), element.getTextTrim());
                }
            }
        });
        return parsedResult;
    }

    public static final Map<String, Charset> ALL_AVAILABLE_CHARSETS = Charset.availableCharsets();

    /**
     * The InputStream cannot be read again after the method invoked.
     */
    public static Charset getFileEncodingByIns(InputStream ins) {
        byte[] buffer = new byte[1024];
        boolean isDetected = false;
        try {
            byte[] cachedBytes = convertToBytes(ins);
            for (Charset detectedCharset : ALL_AVAILABLE_CHARSETS.values()) {
                try (BufferedInputStream bufferIns = new BufferedInputStream(new ByteArrayInputStream(cachedBytes))) {
                    CharsetDecoder charsetDecoder = detectedCharset.newDecoder().reset();
                    while ((bufferIns.read(buffer) != -1)) {
                        isDetected = detectCharset(buffer, charsetDecoder);
                        if (isDetected) {
                            return detectedCharset;
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    /**
     * InputStream is not reusable, you can convert it to byte array
     * to realize reusable goal.
     */
    public static byte[] convertToBytes(InputStream ins) {
        return convertToReusableStream(ins).toByteArray();
    }

    private static ByteArrayOutputStream convertToReusableStream(InputStream ins) {
        byte[] buffer = new byte[1024];
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int length;
            while ((length = ins.read(buffer)) > -1) {
                baos.write(buffer, 0, length);
            }
            baos.flush();
            return baos;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean detectCharset(byte[] bytes, CharsetDecoder charsetDecoder) {
        try {
            charsetDecoder.decode(ByteBuffer.wrap(bytes));
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }
}
