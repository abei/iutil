package com.qinf.study.iutil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2020-03-05.
 */
public class MultiMap<K, V> {
    private List<Entry> table;
    private int initialCapacity = 16;

    public MultiMap() {
        table = new ArrayList<>();
    }

    public MultiMap(int capacity) {
        this.initialCapacity = capacity;
        table = new ArrayList<>(initialCapacity);
    }

    public void put(K key, V value) {
        Objects.requireNonNull(key, "The key of multi map can not be null.");
        Objects.requireNonNull(value, "The value of multi map can not be null.");
        Entry<K, V> entry = new Entry<K, V>(key, value);
        table.add(entry);
    }

    /**
     * If necessary, you can customize toString method of key and
     * value. For instance, the key or the value is not boxed object
     * of primitive value, whereas just a complicated object.
     * Currently, the function can fulfill my basic requirement.
     */
    public String toString() {
        return table.stream()
                .map(entry -> this.wrapQuote(entry.key.toString())
                        .concat(":")
                        .concat(this.wrapQuote(entry.value.toString())))
                .collect(Collectors.joining(","));
    }

    public String toJsonString() {
        String content = table.stream()
                .map(entry -> this.wrapQuote(entry.key.toString())
                        .concat(":")
                        .concat(this.wrapQuote(entry.value.toString())))
                .collect(Collectors.joining(","));
        return new StringBuilder("{").append(content).append("}").toString();
    }

    private String wrapQuote(String content) {
        return new StringBuilder("\"")
                .append(content)
                .append("\"").toString();
    }

    static class Entry<K, V> {
        K key;
        V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
