package com.qinf.study.iutil;

/**
 * Util class used as local cache, meanwhile, it can guarantee cached
 * values are thread safe. The inner cache is ThreadLocal.
 * You should pay attention an issue that the second set operation will overrid
 * the first set operation in a same thread when use this tool Pileline, you can
 * specify a inner map in Pipeline and fix the key type of the map to force invoker
 * use appropriate key to void the override issue.
 *
 * Created by qinf on 2020-03-05.
 */
public class Pipeline {

    private static final ThreadLocal transfer = new ThreadLocal();

    public static <V> V getAndClear() {
        try {
            return (V) transfer.get();
        } finally {
            transfer.remove();
        }
    }

    public static <V> void set(V value) {
        transfer.set(value);
    }

    public static <V> V get() {
        return (V) transfer.get();
    }

    public static void clear() {
        transfer.remove();
    }
}
